package com.example.nam.myapplication;


import android.app.Activity;
import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.graphics.Point;

import android.os.AsyncTask;
import android.os.IBinder;

import android.provider.CalendarContract;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.txusballesteros.bubbles.BubbleLayout;
import com.txusballesteros.bubbles.BubblesManager;
import com.txusballesteros.bubbles.OnInitializedCallback;

import java.net.ProtocolException;
import java.net.MalformedURLException;
import java.io.IOException;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import java.net.URL;
import java.net.URLEncoder;
import java.util.Calendar;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;


/**
 * Created by Nam on 2016-05-16.
 */

public class ChatHeadService extends Service {

    private String urlPath = "http://210.115.170.166/old/";
    private WindowManager windowManager;
    private ImageView chatHead;
    private static ChatHeadService instance=null;
    private String strUrl,strCookie,result;
    private URL Url;
    private String encodeData;
    public static Context mContext;
    public static String TAG = "return activity";
    public static Intent intent = null;
    ToggleButton toggle;
    public static int singleCheck = 0;
    public static int checkHead = 0;

    String local = "장소";
    String title = "내용";
    String date = "날짜";

    int clock,day,minutes,week,month,year;


    int extraTodayE,extraTE,extraTTE,extraNWE,extraNME, extraMax;

    public static int TgOn = 910406;
    public static int TgOff = 8396;

    private BubblesManager bubblesManager;
    private BubbleLayout bubbleView;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
        //return mBinder;
    }

    protected void onHandleIntent(Intent intent){

    }

    @Override
    public void onCreate() {
        super.onCreate();


        singleCheck++;
        Log.e("dbCreate2", "" + singleCheck);


        intent = new Intent(getApplication(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.bubble_layout, null);

        initializeBubblesManager();

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addNewBubble();
            }
        });

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        Display display = windowManager.getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);


        ClipboardManager clipboard = (ClipboardManager) this.getSystemService(Context.CLIPBOARD_SERVICE);
        clipboard.addPrimaryClipChangedListener(mPrimaryChangeListener);


    }

    public void addNewBubble() {
        bubbleView = (BubbleLayout)LayoutInflater.from(this).inflate(R.layout.bubble_layout, null);
        bubbleView.setOnBubbleRemoveListener(new BubbleLayout.OnBubbleRemoveListener() {
            @Override
            public void onBubbleRemoved(BubbleLayout bubble) {
                checkHead--;
                MainActivity.checkHead=-1;
            }
        });
        bubbleView.setOnBubbleClickListener(new BubbleLayout.OnBubbleClickListener() {

            @Override
            public void onBubbleClick(BubbleLayout bubble) {

                startActivity(intent);

            }
        });
        bubbleView.setShouldStickToWall(true);
        bubblesManager.addBubble(bubbleView, 60, 20);
    }

    private void initializeBubblesManager() {
        bubblesManager = new BubblesManager.Builder(this)
                .setTrashLayout(R.layout.bubble_trash_layout)
                .setInitializationCallback(new OnInitializedCallback() {
                    @Override
                    public void onInitialized() {
                        addNewBubble();
                    }
                })
                .build();
        bubblesManager.initialize();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){

        if(checkHead == -1) {
            bubblesManager.addBubble(bubbleView, 60, 20);
            checkHead++;
        }
        return START_REDELIVER_INTENT;

    }


        public ClipboardManager.OnPrimaryClipChangedListener mPrimaryChangeListener = new ClipboardManager.OnPrimaryClipChangedListener() { // nam
        public void onPrimaryClipChanged() {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
           String pasteData = "";
            ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
           pasteData = item.getText().toString();
            String target= TabFragment2.getTarget();

                Log.e("trans","method");
                if(MainActivity.FUNCTION_KIND == 0) {

                    try {
                        Calendar(urlPath, pasteData);
                    } catch(Exception e){
                        Toast.makeText(ChatHeadService.this, "일정추가가 이루어지지 않았습니다. \n텍스트 형식을 확인해주세요", Toast.LENGTH_SHORT).show();
                    }
                }

                else if(MainActivity.FUNCTION_KIND == 1) {
                    Log.e("transInput","true");
                    Translate(urlPath, pasteData, target);
                }

                else if(MainActivity.FUNCTION_KIND == 2 && TabFragment3.getDicKind()==0){
                    Dictionary(urlPath, pasteData);
                }
                else if(MainActivity.FUNCTION_KIND == 2 && TabFragment3.getDicKind()==1){
                    Dictionary2(urlPath, pasteData);
                }
                else if(MainActivity.FUNCTION_KIND == 3 && TabFragment4.getNewsKind()==0){
                    News(urlPath, pasteData);

                }
                else if(MainActivity.FUNCTION_KIND == 3 && TabFragment4.getNewsKind()==1){
                    News2(urlPath, pasteData);

                }
                else {
                    MainActivity.FUNCTION_KIND = PasteActivity.a;
                    Log.e("transResult", "" + MainActivity.FUNCTION_KIND);
                }

            }
    };



    @Override
    public void onDestroy() {
        super.onDestroy();
        singleCheck--;
        if (chatHead != null) windowManager.removeView(chatHead);
        stopSelf();

    }

    void News2(final String urlPath, final String pasteData2)
    {


        new AsyncTask<Void,Void,Void>(){
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                try {
                    encodeData = URLEncoder.encode(pasteData2, "EUC-KR");
                }catch(Exception e)
                {

                }
                strUrl = "http://search.daum.net/search?w=news&nil_search=btn&DA=NTB&enc=utf8&cluster=y&cluster_page=1&q=" + encodeData; //탐색하고 싶은 URL이다.
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try{
                    Url = new URL(strUrl);  // URL화 한다.
                    HttpURLConnection conn = (HttpURLConnection) Url.openConnection(); // URL을 연결한 객체 생성.
                    conn.setRequestMethod("GET"); // get방식 통신
                    conn.setDoOutput(true);       // 쓰기모드 지정
                    conn.setDoInput(true);        // 읽기모드 지정
                    conn.setUseCaches(false);     // 캐싱데이터를 받을지 안받을지
                    conn.setDefaultUseCaches(false); // 캐싱데이터 디폴트 값 설정

                    strCookie = conn.getHeaderField("Set-Cookie"); //쿠키데이터 보관

                    InputStream is = conn.getInputStream();        //input스트림 개방

                    Document doc = Jsoup.connect(strUrl).get();
                    Elements elements = doc.select(".wrap_cont");
                    Log.e("elements", elements.toString());
                    //#content .thmb_lst_area .thumb_lst
                    StringBuilder builder = new StringBuilder();   //문자열을 담기 위한 객체
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"UTF-8"));  //문자열 셋 세팅

                    for(Element element : elements)
                    {
                        builder.append(element.text()+ "\n\n");
                    }


                    result = builder.toString();
                    //result = result.replaceAll("<[^>]*>", "");
                    //result = builder.toString();

                }catch(MalformedURLException | ProtocolException  exception) {
                    exception.printStackTrace();
                }catch(IOException io){
                    io.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Intent PasteActivityIntent = new Intent(getApplication(), PasteActivity.class);
                PasteActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                PasteActivityIntent.putExtra("response",result);
                startActivity(PasteActivityIntent);
            }
        }.execute();

    }
    void News(final String urlPath, final String pasteData2)
    {


        new AsyncTask<Void,Void,Void>(){
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                try {
                    encodeData = URLEncoder.encode(pasteData2, "EUC-KR");
                }catch(Exception e)
                {

                }
                strUrl = "http://news.naver.com/main/search/search.nhn?query=" + encodeData; //탐색하고 싶은 URL이다.
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try{
                    Url = new URL(strUrl);  // URL화 한다.
                    HttpURLConnection conn = (HttpURLConnection) Url.openConnection(); // URL을 연결한 객체 생성.
                    conn.setRequestMethod("GET"); // get방식 통신
                    conn.setDoOutput(true);       // 쓰기모드 지정
                    conn.setDoInput(true);        // 읽기모드 지정
                    conn.setUseCaches(false);     // 캐싱데이터를 받을지 안받을지
                    conn.setDefaultUseCaches(false); // 캐싱데이터 디폴트 값 설정

                    strCookie = conn.getHeaderField("Set-Cookie"); //쿠키데이터 보관

                    InputStream is = conn.getInputStream();        //input스트림 개방

                    Document doc = Jsoup.connect(strUrl).get();
                    Elements elements = doc.select(".srch_lst");
                //#content .thmb_lst_area .thumb_lst
                    StringBuilder builder = new StringBuilder();   //문자열을 담기 위한 객체
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is,"UTF-8"));  //문자열 셋 세팅

                    for(Element element : elements)
                    {
                        builder.append(element.text()+ "\n\n");
                    }


                    result = builder.toString();
                    //result = result.replaceAll("<[^>]*>", "");
                    //result = builder.toString();

                }catch(MalformedURLException | ProtocolException  exception) {
                    exception.printStackTrace();
                }catch(IOException io){
                    io.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Intent PasteActivityIntent = new Intent(getApplication(), PasteActivity.class);
                PasteActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                PasteActivityIntent.putExtra("response",result);
                startActivity(PasteActivityIntent);
            }
        }.execute();


    }

    void Dictionary(final String urlPath, final String pasteData2)
    {


        new AsyncTask<Void,Void,Void>(){
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                try {
                    encodeData = URLEncoder.encode(pasteData2, "UTF-8");
                }catch(Exception e)
                {

                }
                strUrl = "http://terms.naver.com/search.nhn?query=" + encodeData; //탐색하고 싶은 URL이다.
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try{
                    Url = new URL(strUrl);  // URL화 한다.
                    HttpURLConnection conn = (HttpURLConnection) Url.openConnection(); // URL을 연결한 객체 생성.
                    conn.setRequestMethod("GET"); // get방식 통신
                    conn.setDoOutput(true);       // 쓰기모드 지정
                    conn.setDoInput(true);        // 읽기모드 지정
                    conn.setUseCaches(false);     // 캐싱데이터를 받을지 안받을지
                    conn.setDefaultUseCaches(false); // 캐싱데이터 디폴트 값 설정

                    strCookie = conn.getHeaderField("Set-Cookie"); //쿠키데이터 보관

                    InputStream is = conn.getInputStream();        //input스트림 개방

                    Document doc = Jsoup.connect(strUrl).get();
                    Elements elements = doc.select("#list .li_w .txt");
                    StringBuilder builder = new StringBuilder();   //문자열을 담기 위한 객체

                    for(Element element : elements)
                    {
                        builder.append(element.text() + "\n\n");



                    }

                    result = builder.toString();
                    result = result.replaceAll("<[^>]*>", "");
                    result = result.replace("&quot;","");
                    result = result.replace("&lt;", "");
                    result = result.replace("&middot;","");
                    result = result.replace("&gt;","");
                    result = result.replace("&copy;","");
                    result = result.replace("&sup3;","");

                    //result = builder.toString();

                }catch(MalformedURLException | ProtocolException  exception) {
                    exception.printStackTrace();
                }catch(IOException io){
                    io.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Intent PasteActivityIntent = new Intent(getApplication(), PasteActivity.class);
                PasteActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                PasteActivityIntent.putExtra("response",result);
                startActivity(PasteActivityIntent);
            }
        }.execute();


    }

    void Dictionary2(final String urlPath, final String pasteData2)
    {

        new AsyncTask<Void,Void,Void>(){
            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                strUrl = "https://ko.wikipedia.org/wiki/" + pasteData2; //탐색하고 싶은 URL이다.
            }

            @Override
            protected Void doInBackground(Void... voids) {
                try{
                    Url = new URL(strUrl);  // URL화 한다.
                    HttpURLConnection conn = (HttpURLConnection) Url.openConnection(); // URL을 연결한 객체 생성.
                    conn.setRequestMethod("GET"); // get방식 통신
                    conn.setDoOutput(true);       // 쓰기모드 지정
                    conn.setDoInput(true);        // 읽기모드 지정
                    conn.setUseCaches(false);     // 캐싱데이터를 받을지 안받을지
                    conn.setDefaultUseCaches(false); // 캐싱데이터 디폴트 값 설정

                    strCookie = conn.getHeaderField("Set-Cookie"); //쿠키데이터 보관

                    InputStream is = conn.getInputStream();        //input스트림 개방

                    Document doc = Jsoup.connect(strUrl).get();
                    Log.e("output", ""+doc);
                    Elements docEle = doc.select(".mw-body p");


                    result = docEle.toString();
                    result = result.replaceAll("<[^>]*>", "");
                    result = result.replace("&quot;","");
                    result = result.replace("&lt;", "");
                    result = result.replace("&middot;","");
                    result = result.replace("&gt;","");
                    result = result.replace("&copy;","");
                    result = result.replace("&sup3;","");


                }catch(MalformedURLException | ProtocolException  exception) {
                    exception.printStackTrace();
                }catch(IOException io){
                    io.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Intent PasteActivityIntent = new Intent(getApplication(), PasteActivity.class);
                PasteActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                PasteActivityIntent.putExtra("response",result);
                startActivity(PasteActivityIntent);
            }
        }.execute();


    }

    void Translate(final String urlPath, String pasteData2, String setTarget) // nam
    {

        try {
            encodeData = URLEncoder.encode(pasteData2, "UTF-8");
        }
        catch(Exception e)
        {

        }

        String pasteData = pasteData2.toString().replace(" ","+");
        pasteData = pasteData.toString().replace("\n", "+");

        DBConnect dbConnect = new DBConnect();
        Log.e("setTarget", "" + setTarget);
        String response = dbConnect.dataReceive2(urlPath + "hotkey2.php?message=" + encodeData + "&setTarget=" + setTarget);
        Log.e("transdata", "" + response);

        Intent PasteActivityIntent = new Intent(getApplication(), PasteActivity.class);
        PasteActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PasteActivityIntent.putExtra("response",response);
        startActivity(PasteActivityIntent);

       // final Toast tst = Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG);
       // tst.show();

    }

    void Calendar(String urlPath, String pasteData)
    {
        ParsingString parsingString = new ParsingString(); // 생성
        parsingString.savedString(pasteData);
        pasteData = pasteData.toString().replace("\n", "%20");
        pasteData = pasteData.toString().replace(" ", "%20");
        Calendar calendar = Calendar.getInstance();
        DBConnect dbConnect = new DBConnect();
        String response = dbConnect.dataReceive(parsingString, urlPath + "hotkey.php?message="+pasteData);
        Log.e("pasteData", "" + response);

        date = response.split(" ")[0];
        date = date.split("시")[0];

        local = response.split(" ")[1];
        local = local.split("에")[0];
        local = local.split("으")[0];
        local = local.split("로")[0];
        local = local.split("으로")[0];

        try {

             title = response.split(" ")[2];
             title = title.split("을")[0];

        } catch(Exception e)
        {
            title = "약속";
        }

        Log.e("date_local_title", ""+date+local+title);


        ChangeFormat(parsingString);

        try{
            Intent calendarIntent = new Intent(Intent.ACTION_INSERT, CalendarContract.Events.CONTENT_URI).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);

            Log.e("month, day, week, NWE", ""+month + " " + day + " " + week + " " + extraNWE);

            // 시작 시간
            Calendar beginTime = Calendar.getInstance();

            if(extraNWE != 0)
            {
                if(week != 0)
                {
                    beginTime.set(Calendar.MONTH, (month == 0) ? (calendar.get(Calendar.MONTH)) + ((extraNME != 0) ? extraNME : 0) : month - 1);
                    beginTime.set(Calendar.DAY_OF_MONTH, (calendar.get(Calendar.DAY_OF_MONTH))); // 일
                    beginTime.add(Calendar.DAY_OF_MONTH, 7);
                    beginTime.set(Calendar.DAY_OF_WEEK,  week);
                    beginTime.set(Calendar.HOUR_OF_DAY, (clock == 0) ? calendar.get(Calendar.HOUR_OF_DAY) : clock);
                    beginTime.set(Calendar.MINUTE, (minutes == 0) ? 0 : minutes);
                }
                else
                {
                    beginTime.set(Calendar.MONTH, (month == 0) ? (calendar.get(Calendar.MONTH)) + ((extraNME != 0) ? extraNME : 0) : month - 1);
                    beginTime.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH)); // 일
                    beginTime.set(Calendar.DAY_OF_WEEK,  calendar.get(Calendar.DAY_OF_WEEK));
                    beginTime.add(Calendar.DAY_OF_MONTH, 7);
                    beginTime.set(Calendar.HOUR_OF_DAY, (clock == 0) ? calendar.get(Calendar.HOUR_OF_DAY) : clock);
                    beginTime.set(Calendar.MINUTE, (minutes == 0) ? 0 : minutes);
                }
            }
            else if( extraTE != 0)
            {
                if(extraTTE!=0)
                {
                    beginTime.set(Calendar.MONTH, (month == 0) ? (calendar.get(Calendar.MONTH)) + ((extraNME != 0) ? extraNME : 0) : month - 1);
                    beginTime.set(Calendar.DAY_OF_MONTH, (calendar.get(Calendar.DAY_OF_MONTH))); // 일
                    beginTime.add(Calendar.DAY_OF_MONTH, 2);
                    beginTime.set(Calendar.HOUR_OF_DAY, (clock == 0) ? calendar.get(Calendar.HOUR_OF_DAY) : clock);
                    beginTime.set(Calendar.MINUTE, (minutes == 0) ? 0 : minutes);
                }
                else
                {
                    beginTime.set(Calendar.MONTH, (month == 0) ? (calendar.get(Calendar.MONTH)) + ((extraNME != 0) ? extraNME : 0) : month - 1);
                    beginTime.set(Calendar.DAY_OF_MONTH, (calendar.get(Calendar.DAY_OF_MONTH))); // 일
                    beginTime.add(Calendar.DAY_OF_MONTH, 1);
                    beginTime.set(Calendar.HOUR_OF_DAY, (clock == 0) ? calendar.get(Calendar.HOUR_OF_DAY) : clock);
                    beginTime.set(Calendar.MINUTE, (minutes == 0) ? 0 : minutes);
                }
            }
            else if( extraTTE != 0)
            {
                beginTime.set(Calendar.MONTH, (month == 0) ? (calendar.get(Calendar.MONTH)) + ((extraNME != 0) ? extraNME : 0) : month - 1);
                beginTime.set(Calendar.DAY_OF_MONTH, (calendar.get(Calendar.DAY_OF_MONTH))); // 일
                beginTime.add(Calendar.DAY_OF_MONTH, 2);
                beginTime.set(Calendar.HOUR_OF_DAY, (clock == 0) ? calendar.get(Calendar.HOUR_OF_DAY) : clock);
                beginTime.set(Calendar.MINUTE, (minutes == 0) ? 0 : minutes);
            }
            else if( week != 0)
            {
                beginTime.set(Calendar.MONTH, (month == 0) ? (calendar.get(Calendar.MONTH)) + ((extraNME != 0) ? extraNME : 0) : month - 1);
                beginTime.set(Calendar.DAY_OF_MONTH, (calendar.get(Calendar.DAY_OF_MONTH))); // 일
                beginTime.add(Calendar.DAY_OF_MONTH, 7);
                beginTime.set(Calendar.DAY_OF_WEEK,  week);
                beginTime.set(Calendar.HOUR_OF_DAY, (clock == 0) ? calendar.get(Calendar.HOUR_OF_DAY) : clock);
                beginTime.set(Calendar.MINUTE, (minutes == 0) ? 0 : minutes);
            }
            else{
                beginTime.set(Calendar.MONTH, (month == 0) ? (calendar.get(Calendar.MONTH)) + ((extraNME != 0) ? extraNME : 0) : month - 1);
                beginTime.set(Calendar.DAY_OF_MONTH, (day == 0) ? ((clock != 0) ? calendar.get(Calendar.DAY_OF_MONTH) : day) : day); // 일
                //beginTime.set(Calendar.DAY_OF_WEEK, (week == 0) ? calendar.get(Calendar.DAY_OF_WEEK) : week);
                beginTime.set(Calendar.HOUR_OF_DAY, (clock == 0) ? calendar.get(Calendar.HOUR_OF_DAY) : clock);
                beginTime.set(Calendar.MINUTE, (minutes == 0) ? 0 : minutes);
            }

            // putExtra로 시작 시간과 끝 시간을 추가
            calendarIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
                    beginTime.getTimeInMillis());

            // putExtra로 event's title과 event's location을 추가
            calendarIntent.putExtra(CalendarContract.Events.TITLE, title);
            calendarIntent.putExtra(CalendarContract.Events.EVENT_LOCATION, local);

            // 만든 intent를 시스템에 넘겨 적절한 앱 호출
            startActivity(calendarIntent);

        } catch (Exception e){
            e.printStackTrace();
        }

    }


    public void ChangeFormat(ParsingString parsingString)
    {
        ParsingString.CheckDate checkDate = parsingString.getAll();




        if(checkDate.clock == "")
           clock = 0;
        else
            clock = Integer.parseInt(checkDate.clock);
        if(checkDate.day == "")
            day = 0;
        else
            day = Integer.parseInt(checkDate.day);
        if(checkDate.minutes == "")
            minutes = 0;
        else
            minutes = Integer.parseInt(checkDate.minutes);
        if(checkDate.month == "")
            month = 0;
        else
            month = Integer.parseInt(checkDate.month);
        if(checkDate.year == "")
            year = 0;
        else
            year = Integer.parseInt(checkDate.year);


        if(checkDate.week.contains("월요일"))
            week = 2;
        else if(checkDate.week.contains("화요일"))
            week = 3;
        else if(checkDate.week.contains("수요일"))
            week = 4;
        else if(checkDate.week.contains("목요일"))
            week = 5;
        else if(checkDate.week.contains("금요일"))
            week = 6;
        else if(checkDate.week.contains("토요일"))
            week = 7;
        else if(checkDate.week.contains("일요일"))
            week = 1;
        else
            week = 0;

        Log.e("checkWeek",""+week);

        if(checkDate.extrasTodayE == "")
            extraTodayE = 0;
        else
            extraTodayE = Integer.parseInt(checkDate.extrasTodayE);

        if(checkDate.extrasTE == "")
            extraTE = 0;
        else
            extraTE = Integer.parseInt(checkDate.extrasTE);

        if(checkDate.extrasTTE == "")
            extraTTE = 0;
        else
            extraTTE = Integer.parseInt(checkDate.extrasTTE);

        if(checkDate.extrasNWE == "")
            extraNWE = 0;
        else
            extraNWE = Integer.parseInt(checkDate.extrasNWE);

        if(checkDate.extrasNME == "")
            extraNME = 0;
        else
            extraNME = Integer.parseInt(checkDate.extrasNME);


        extraMax = 0;

        extraMax = (extraTodayE != 0) ? extraTodayE : extraMax;
        extraMax = (extraTE != 0) ? extraTE : extraMax;
        extraMax = (extraTTE != 0) ? extraTTE : extraMax;
        extraMax = (extraNWE != 0) ? extraNWE : extraMax;

    }


}

/* 149
af Afrikaans
ak Akan
sq Albanian
am Amharic
ar Arabic
hy Armenian
az Azerbaijani
eu Basque
be Belarusian
bem Bemba
bn Bengali
bh Bihari
xx-bork Bork, bork, bork!
bs Bosnian
br Breton
bg Bulgarian
km Cambodian
ca Catalan
chr Cherokee
ny Chichewa
zh-CN Chinese (Simplified)
zh-TW Chinese (Traditional)
co Corsican
hr Croatian
cs Czech
da Danish
nl Dutch
xx-elmer Elmer Fudd
en English
eo Esperanto
et Estonian
ee Ewe
fo Faroese
tl Filipino
fi Finnish
fr French
fy Frisian
gaa Ga
gl Galician
ka Georgian
de German
el Greek
gn Guarani
gu Gujarati
xx-hacker Hacker
ht Haitian Creole
ha Hausa
haw Hawaiian
iw Hebrew
hi Hindi
hu Hungarian
is Icelandic
ig Igbo
id Indonesian
ia Interlingua
ga Irish
it Italian
ja Japanese
jw Javanese
kn Kannada
kk Kazakh
rw Kinyawanda
rn Kirundi
xx-klingon Klingon
kg Kongo
ko Korean
kri Krio (Sierra Leone)
ku Kurdish
ckb Kurdish (Sorani)
ky Kyrgyz
lo Laothian
la Latin
lv Latvian
ln Lingala
lt Lithuanian
loz Lozi
lg Luganda
ach Luo
mk Macedonian
mg Malagasy
ms Malay
ml Malayalam
mt Maltese
mi Maori
mr Marathi
mfe Mauritian Creole
mo Moldavian
mn Mongolian
sr-ME Montenegrin
ne Nepali
pcm Nigerian Pidgin
nso Northern Sotho
no Norwegian
nn Norwegian (Nynorsk)
oc Occitan
or Oriya
om Oromo
ps Pashto
fa Persian
xx-pirate Pirate
pl Polish
pt-BR Portuguese (Brazil)
pt-PT Portuguese (Portugal)
pa Punjabi
gu Quechua
ro Romanian
rm Romansh
nyn Runyakitara
ru Russian
gd Scots Gaelic
sr Serbian
sh Serbo-Croatian
st Sesotho
tn Setswana
crs Seychellois Creole
sn Shona
sd Sindhi
si Sinhalese
sk Slovak
sl Slovenian
so Somali
es Spanish
es-419 Spanish (Latin American)
su Sundanese
sw Swahili
sv Swedish
tg Tajik
ta Tamil
tt Tatar
te Telugu
th Thai
ti Tigrinya
to Tonga
lua Tshiluba
tum Tumbuka
tr Turkish
tk Turkmen
tw Twi
ug Uighur
uk Ukrainian
ur Urdu
uz Uzbek
vi Vietnamese
cy Welsh
wo Wolof
xh Xhosa
yi Yiddish
yo Yoruba
zu Zulu
*/
