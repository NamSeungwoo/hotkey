package com.example.nam.myapplication;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

/**
 * Created by Nam on 2016-09-20.
 */
public class PasteActivity extends ActionBarActivity {
    private String response;
    public static int a=-1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.paste_activity);
        TextView pasteView = (TextView) findViewById(R.id.pasteView);
        Button pasteButton1 = (Button) findViewById(R.id.pasteButton1);
        Button pasteButton2 = (Button) findViewById(R.id.pasteButton2);

        Intent intent = getIntent();
        response = intent.getExtras().getString("response");
        pasteView.setText("" + response);


        pasteButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                a = MainActivity.FUNCTION_KIND;
                MainActivity.FUNCTION_KIND = -1;
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData.Item pasteItem = new ClipData.Item(response);
                String[] mimeType = new String[1];
                mimeType[0] = ClipDescription.MIMETYPE_TEXT_URILIST;
                ClipData cd = new ClipData(new ClipDescription("text_data", mimeType), pasteItem);
                clipboard.setPrimaryClip(cd);

            }
        });

        pasteButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });

    }

}
