package com.example.nam.myapplication;

import android.os.AsyncTask;
import android.os.StrictMode;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class DBConnect {
    public static HttpURLConnection connection;
    private URL url;
    private String receive;

    public DBConnect() {

    }

    public String dataReceive(ParsingString parsingString, String url) {
        connection(url);
        receive = null;
        try {
            Log.e("2","1st");
            BufferedReader bf = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
            Log.e("2", "2nd");
            StringBuilder string = new StringBuilder();
            Log.e("2", "3rd");
            String line;

            int i=0;
            while ((line = bf.readLine()) != null) {


                String temp = line.split("\n")[0];
                parsingString.add(i,temp);
                Log.e("DBConnect",temp);
                string.append(temp +" ");
                i++;
            }
            Log.e("parsingString","in");
            receive = parsingString.Check(i);
            Log.e("parsingString","out");
            Log.e("resceive", receive);
            //receive = string.toString().split("> ")[1];

        } catch (Exception e) {
            receive = "error";
        }finally{
            //connection.disconnect();
            //connection = null;
        }
        connection.disconnect();
        connection = null;
        return receive;
    }

    public String dataReceive2(String url) {
        connection(url);
        receive = null;
      //  String temp;
        try {
            Log.e("1","1st");
            BufferedReader bf = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
            Log.e("1","2nd");
            StringBuilder string = new StringBuilder();
            Log.e("1","3rd");
            String line;
            String[] array;
//            line = bf.readLine();
//            temp = line.split("\t")[0];

            while ((line = bf.readLine()) != null){


                array = line.split("%20");
                for (int i=0; i<array.length; i++) {
                    string.append(array[i] + " ");
                }
                Log.e("DBConnect", string.toString());

            }

            receive = string.toString();

        } catch (Exception e) {
            receive = "서버가 불안정해서 번역을 할 수 없습니다.";
        } finally
        {
           // connection.disconnect();
           // connection = null;
        }
        connection.disconnect();
        connection = null;
        return receive;
    }


    private void connection(String url) {
        try {
            passiveMethod();
            this.url = new URL(url);
            this.connection = (HttpURLConnection) this.url.openConnection();

            connection.setDefaultUseCaches(false);
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setRequestProperty("content-type", "application/x-www-form-urlencoded");
            connection.setRequestMethod("POST");
        } catch (Exception e) {
            e.printStackTrace();
        } finally{
            connection.disconnect();
        }

    }

    static public void passiveMethod() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }
}