package com.example.nam.myapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Created by Nam on 2016-10-14.
 */
public class TabFragment1 extends Fragment {
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){

        View view = inflater.inflate(R.layout.tab_fragment_1, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.schedule_img);
        imageView.setImageResource(R.drawable.schedule);

        Button button =(Button) view.findViewById(R.id.schedule_button);
        MainActivity.FUNCTION_KIND = -1;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(MainActivity.TgId == MainActivity.TgOff)
                    Toast.makeText(getActivity(),"ON 버튼으로 활성화 하세요",Toast.LENGTH_SHORT).show();
                else if(MainActivity.checkHead < 0)
                    Toast.makeText(getActivity(),"LAUNCH HOTKEY로 핫키를 띄워주세요",Toast.LENGTH_SHORT).show();
                else if(MainActivity.TgId == MainActivity.TgOn) {
                    MainActivity.FUNCTION_KIND = 0;
                    getActivity().finish();
                }

            }
        });

    return view;

    }
}
