package com.example.nam.myapplication;

import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

/**
 * Created by Nam on 2016-10-14.
 */
public class TabFragment2 extends Fragment {
    private static String target ="";

    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.tab_fragment_2, container, false);

        Button button = (Button) view.findViewById(R.id.translation_button);
        ImageView imageView = (ImageView) view.findViewById(R.id.translate_img);
        imageView.setImageResource(R.drawable.translate);

        MainActivity.FUNCTION_KIND = -1;
        setTarget("en");

        final RadioButton en = (RadioButton) view.findViewById(R.id.en);
        final RadioButton ko = (RadioButton) view.findViewById(R.id.ko);
        final RadioButton zhCN = (RadioButton) view.findViewById(R.id.zhCN);
        final RadioButton ja = (RadioButton) view.findViewById(R.id.ja);
        final RadioButton fr = (RadioButton) view.findViewById(R.id.fr);
        RadioButton.OnClickListener languageOnClickListener = new RadioButton.OnClickListener() {
            public void onClick(View v){
                if(en.isChecked())
                    setTarget("en");
                else if(ko.isChecked())
                    setTarget("ko");
                else if(zhCN.isChecked())
                    setTarget("zh-CN");
                else if(ja.isChecked())
                    setTarget("ja");
                else if(fr.isChecked())
                    setTarget("fr");
            }
        };


        en.setOnClickListener(languageOnClickListener);
        ko.setOnClickListener(languageOnClickListener);
        zhCN.setOnClickListener(languageOnClickListener);
        ja.setOnClickListener(languageOnClickListener);
        fr.setOnClickListener(languageOnClickListener);
        en.setChecked(true);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(MainActivity.TgId == MainActivity.TgOff)
                    Toast.makeText(getActivity(),"ON 버튼으로 활성화 하세요",Toast.LENGTH_SHORT).show();
                else if(MainActivity.checkHead < 0)
                    Toast.makeText(getActivity(),"LAUNCH HOTKEY로 핫키를 띄워주세요",Toast.LENGTH_SHORT).show();
                else if(MainActivity.TgId == MainActivity.TgOn) {
                    MainActivity.FUNCTION_KIND = 1;
                    getActivity().finish();
                }
            }
        });
        return view;
    }
    public static String getTarget(){

        return target;
    }
    private void setTarget(String _target){
        target = _target;
    }
}
