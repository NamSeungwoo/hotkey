package com.example.nam.myapplication;

import android.util.Log;

/**
 * Created by Nam on 2016-09-26.
 */
public class ParsingString {


    StringBuilder stringBuilder = new StringBuilder();
    String[] arr = new String[100];
    String saved = "";
    String[] array;
    CheckDate date = new CheckDate();

    boolean checkContent=false;
    boolean checkLocation=false;
    String checkingContent="";

    public void arrayCheck()
    {
        for(int i=0; i<array.length; i++)
        {
            Log.e("arrayContent", ""+array[i]);
            switch(array[i])
            {

                case "오늘부터":
                case "오늘":
                case "오늘까지":
                    date.extrasTodayE = "0";
                    break;
                case "다음주부터":
                case "다음 주부터":
                case "다음주":
                case "다음 주":
                case "다음주에":
                case "다음 주에:":
                case "다음주까지":
                case "다음 주까지":
                    date.extrasNWE = "7";
                    break;
                case "다음달부터":
                case "다음 달부터":
                case "다음달":
                case "다음 달":
                case "다음달에":
                case "다음 달에":
                case "다음달까지":
                case "다음 달까지":
                    date.extrasNME= "1";
                    break;
                case "내일부터":
                case "내일":
                case "내일까지":
                    date.extrasTE = "1";
                    break;
                case "모레부터":
                case "모레까지":
                case "모레":
                case "모레에":
                    date.extrasTTE = "2";
                    break;
                default:
                    break;

            }
        }
    }

    public String LocCheck(String temp)
    {
        temp = temp.split("\t")[0];
        for(int i=0; i<array.length; i++)
        {
            if(array[i].contains(temp))
                return array[i];
        }
        return temp;
    }


    public String Check(int n) // 형태소로 쪼개진거
    {
        StringBuilder result= new StringBuilder();
        String temp="";
        String loc="";
        String time="";
        String content="";

        Log.e("inloop", "before");
        for(int i=1; i<n; i++) {
            Log.e("string",""+arr[i]);


            if ((temp = checkLoc(i, arr[i])) != "empty")
            {
                Log.e("Loc",temp);
                loc = temp;

            }
            else if ((temp = checkTime(i, arr[i])) != "empty")
            {
                Log.e("time",temp);
                time = temp;

                if(time.contains("년"))
                    date.year = temp.split("년")[0];
                else if(time.contains("월"))
                    date.month = temp.split("월")[0];
                else if(time.contains("일"))
                    date.day = temp.split("일")[0];
                else if(time.contains("시"))
                    date.clock = temp.split("시")[0];
                else if(time.contains("분"))
                    date.minutes = temp.split("분")[0];

                Log.e("maybe","here");
            }
            else if ((temp = checkContent(i, arr[i], content)) != "empty")
            {
                Log.e("content",temp);
                content = temp;
            }



            if((arr[i].contains("요일")))
            {
                date.week = arr[i].split("\t")[0];
            }



        }
        result.append(time);
        result.append(" ");
        result.append(loc);
        result.append(" ");
        result.append(content);

        arrayCheck();
        Log.e("checkDate", "1."+date.year+ "2."+date.month+ "3."+date.day+ "4." + date.week+ "5." + date.extrasNME + "6." + date.extrasNWE + "7." + date.extrasTE + "8." + date.extrasTodayE
                + "9." + date.extrasTTE+ "10." +date.clock+ "11."+date.minutes+ " ");
        Log.e("result", ""+result);
        return result.toString();
    }

    public String SplitString(int i, int n, String temp, int cut)
    {
        String a="";
        //String b="";
        StringBuilder result= new StringBuilder();

        for(int j=n; j>=cut; j--) {    // i = 4, n = 2
            a = arr[i - j].split("\t")[0];
            result.append(a);
        }
        //Log.e("resultTemp",result.toString());
        return result.toString();
    }

    public String checkLoc(int i, String temp) {
        if (temp.contains("JKB")) {
            if(arr[i+1].contains("JX") && arr[i-1].contains("NNG"))
                return LocCheck(temp);
            else if(arr[i-1].contains("SL"))
                return LocCheck(temp);
            else if(arr[i-1].contains("NNG")&&arr[i-2].contains("XSN")&&arr[i-3].contains("NNG"))
                return SplitString(i,3,temp,1);
            else if(arr[i-1].contains("NNG")&&arr[i-2].contains("XSN")&&arr[i-3].contains("NP"))
                return SplitString(i,3,temp,1);
            else if(arr[i-1].contains("NNG") && arr[i-2].contains("JKS") && arr[i-3].contains("NP"))
                return SplitString(i,3,temp,1);
            else if(arr[i-1].contains("NNG") && arr[i-2].contains("MM") && arr[i-3].contains("JKS") && arr[i-4].contains("NNG"))
                return SplitString(i,4,temp,1);
            else if (arr[i - 1].contains("NNG") && arr[i - 2].contains("XSN"))
                return SplitString(i,2,temp,1);
            else if (arr[i-1].contains("NNG") && arr[i-2].contains("NNP")) {
                return SplitString(i, 2, temp, 1);
            }
            else if(arr[i-1].contains("NNG") && arr[i-2].contains("XPN")){
                return SplitString(i,2,temp,1);
            }
            else if (arr[i - 1].contains("NNG")) {
                //Log.e("check",temp);
                return SplitString(i, 1, temp,1);
            }
            else if (arr[i-1].contains("NNP"))
                return SplitString(i,1,temp,1);
            else if (arr[i-1].contains("NNBC") && arr[i-2].contains("SN") && arr[i-3].contains("SL"))
                return SplitString(i,3,temp,1);
            else if (arr[i-1].contains("NNBC") && arr[i-2].contains("SN") && arr[i-3].contains("NNG"))
                return SplitString(i,3,temp,1);

        }

        if(checkLocation == false && ((checkingContent != temp && temp.contains("NNG")||(checkingContent != temp && temp.contains("NNP"))))){
            checkLocation=true;
            return LocCheck(temp);
            //return SplitString(i,0,temp,0);
        }
        return "empty";
    }
    public String checkTime(int i, String temp) {
        if (temp.contains("JKB")) {
            if((arr[i - 1].contains("NNBC") &&arr[i-2].contains("SN")) && (arr[i-3].contains("Compound")||arr[i-3].contains("내일")||arr[i-3].contains("오늘")||arr[i-3].contains("모레")||arr[i-3].contains("주")||arr[i-3].contains("달")))
                return SplitString(i, 2, temp,2);
            if((arr[i - 1].contains("NNBC") &&arr[i-2].contains("SN") && arr[i-3].contains("NNP")) ||
                    (arr[i - 1].contains("NNBC") &&arr[i-2].contains("SN") && arr[i-3].contains("NNG")) ||
                    (arr[i - 1].contains("NNBC") &&arr[i-2].contains("SN") && arr[i-3].contains("SL"))
                    )
                return "empty";
            if (arr[i - 1].contains("NNBC") && arr[i - 2].contains("SN"))
                return SplitString(i, 2, temp,2);
        } else if (temp.contains("NNBC"))
        {
            if(arr[i-1].contains("SN") && (arr[i-2].contains("Compound")||arr[i-2].contains("내일")||arr[i-2].contains("오늘")||arr[i-2].contains("모레")||arr[i-2].contains("주") || arr[i-2].contains("달")))
                return SplitString(i,1,temp,0);
            if((arr[i-1].contains("SN") && arr[i-2].contains("NNP")) ||
                    (arr[i-1].contains("SN") && arr[i-2].contains("NNG")) ||
                    (arr[i-1].contains("SN") && arr[i-2].contains("SL"))
            )
                return "empty";
            if(arr[i-1].contains("SN"))
                return SplitString(i,1,temp,0);
        }
        else if(temp.contains("JK"))
        {
            if(arr[i-1].contains("NNBC") && arr[i-2].contains("SN"))
                return SplitString(i,2,temp,2);
        }

        return "empty";
    }
    public String checkContent(int i, String temp, String content)
    {
        if(checkContent==false && temp.contains("VV+EC")) {
        return checkingContent = SplitString(i,0,temp,0);
      }

        else if(checkContent==false && temp.contains("NNG")) {
            if(!arr[i+1].contains("JKB")) {
                Log.e("checkContent", temp);
                return checkingContent = SplitString(i, 0, temp, 0);
            }
        }

        else if(checkContent==false && temp.contains("EC"))
        {
            if((arr[i-1].contains("VV") || arr[i-1].contains("EF"))&& arr[i-2].contains("NNG")) {
                checkContent=true;
                return checkingContent = SplitString(i, 2, temp, 2);
            }
            else if((arr[i-1].contains("VV")) || arr[i-1].contains("VV")) {
                checkContent=true;
                return checkingContent = SplitString(i,1,temp,0);
            }

            else if(arr[i-1].contains("XSV") && arr[i-2].contains("NNG")) {
                checkContent=true;
                return checkingContent = SplitString(i, 2, temp, 2);
            }
            else if(arr[i-1].contains("NNG")) {
                checkContent=true;
                return checkingContent = SplitString(i, 1, temp, 1);
            }
        }
        else if(checkContent==false && temp.contains("EF")) {
            if (arr[i - 1].contains("VA") && arr[i - 2].contains("NNG")) {
                checkContent = true;
                return checkingContent = SplitString(i, 2, temp, 2);
            } else if (arr[i - 1].contains("EP+EP") || arr[i - 1].contains("VV+EP") || arr[i - 1].contains("XSV+EP")) {
                if (arr[i - 1].contains("EP+EP") && arr[i - 2].contains("XSV")) {
                    checkContent = true;
                    return checkingContent = SplitString(i, 3, temp, 3);
                }

                if (arr[i - 2].contains("JKB"))
                    return "empty";
                checkContent = true;
                return checkingContent = SplitString(i, 2, temp, 2);
            }

        }
        else if(checkContent==false && temp.contains("JKO"))
        {
            checkContent=true;
            //return SplitString(i,1,temp,1);
            return LocCheck(temp);
        }
        else if(checkContent==false && temp.contains("VV")) {
            return checkingContent = SplitString(i,0,temp,0);
        }

        return "empty";
    }
    public void add(int n, String temp)
    {
        arr[n] = temp;
    }

    public void savedString(String original)
    {
        saved = original;
        array = saved.split(" ");
    }
    public CheckDate getAll()
    {
        return date;
    }

    class CheckDate
    {
        public String year="";
        public String month="";
        public String day="";
        public String week="";
        public String clock="";
        public String minutes="";
        public String extrasNWE="";
        public String extrasNME="";
        public String extrasTTE="";
        public String extrasTE="";
        public String extrasTodayE="";


    }
}
