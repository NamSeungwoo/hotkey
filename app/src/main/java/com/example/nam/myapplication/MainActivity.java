package com.example.nam.myapplication;
import android.app.Activity;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.content.pm.ActivityInfo;
import android.support.design.widget.TabLayout;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;

import android.graphics.PixelFormat;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.IBinder;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.txusballesteros.bubbles.BubbleLayout;
import com.txusballesteros.bubbles.BubblesManager;
import com.txusballesteros.bubbles.OnInitializedCallback;


public class MainActivity extends AppCompatActivity {

    ToggleButton toggle;
    boolean on;
    private static Bundle bundle = new Bundle();
    private ClipboardManager myClipboard;
    public static int TgOn = 910406;
    public static int TgOff = 8396;
    public static int TgId;
    public static int FUNCTION_KIND=-1;
    public static Intent serviceIntent = null;
    public static int singleCheck = 0;
    private static int initiate=-1;
    public static int checkHead = -1;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    //public static Intent serviceIntent;
    private ChatHeadService mService;
    private boolean mBound = false;
    public static Context mContext;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.rcvr_toolbar);
        setSupportActionBar(toolbar);



        Log.e("serviceIntent", "" + serviceIntent);



        tabLayout = (TabLayout) findViewById(R.id.rcvr_tl_tabs);
        tabLayout.addTab(tabLayout.newTab().setText("공지"));
        tabLayout.addTab(tabLayout.newTab().setText("스케줄"));
        tabLayout.addTab(tabLayout.newTab().setText("번역"));
        tabLayout.addTab(tabLayout.newTab().setText("사전"));
        tabLayout.addTab(tabLayout.newTab().setText("뉴스"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);



        viewPager = (ViewPager) findViewById(R.id.rcvr_vp_pager);

        PagerAdapter pagerAdapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                MainActivity.FUNCTION_KIND = tab.getPosition();
            }

            public void onTabUnselected(TabLayout.Tab tab) {

            }

            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        toggle = (ToggleButton)findViewById(R.id.toggleBtn);


        toggle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setToggle();
            }
        });
        myClipboard = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);

        singleCheck++;
        Log.e("dbCreate",""+singleCheck);





    }



    public void onToggleClicked(View view)
    {
        on = ((ToggleButton) view).isChecked();

    }

    @Override
    public void onPause() {
        super.onPause();
        bundle.putBoolean("ToggleButtonState", toggle.isChecked());
        setToggle();


    }

    @Override
    public void onResume()
    {
        Log.e("onResume", "true");

      super.onResume();

        Log.e("onResumeInit", ""+initiate);
        toggle.setText("OFF");
        setToggle();
        initiate++;

    }


    @Override
    protected void onDestroy() {
        singleCheck--;
        super.onDestroy();

        //myClipboard.removePrimaryClipChangedListener(mService.mPrimaryChangeListener);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public void setToggle() {
        if(toggle.getText().toString().equalsIgnoreCase("ON"))
        {
            TgId = TgOn;
        }
        else
        {
            TgId = TgOff;
            MainActivity.FUNCTION_KIND=-1;
        }
    }
}

