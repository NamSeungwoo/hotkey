package com.example.nam.myapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

/**
 * Created by Nam on 2016-10-14.
 */
public class TabFragment3 extends Fragment {
    private static int dicKind=-1;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.tab_fragment_3, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.dictionary_img);
        imageView.setImageResource(R.drawable.dictionary);

        Button button = (Button) view.findViewById(R.id.dictionary_button);
        MainActivity.FUNCTION_KIND = -1;
        setDicKind(0);

        final RadioButton naver = (RadioButton) view.findViewById(R.id.naver);
        final RadioButton wiki = (RadioButton) view.findViewById(R.id.wiki);

        RadioButton.OnClickListener languageOnClickListener = new RadioButton.OnClickListener() {
            public void onClick(View v){
                if(naver.isChecked())
                    setDicKind(0);
                else if(wiki.isChecked())
                    setDicKind(1);

            }
        };

        naver.setOnClickListener(languageOnClickListener);
        wiki.setOnClickListener(languageOnClickListener);
        naver.setChecked(true);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(MainActivity.TgId == MainActivity.TgOff)
                    Toast.makeText(getActivity(),"ON 버튼으로 활성화 하세요",Toast.LENGTH_SHORT).show();
                else if(MainActivity.checkHead < 0)
                    Toast.makeText(getActivity(),"LAUNCH HOTKEY로 핫키를 띄워주세요",Toast.LENGTH_SHORT).show();
                else if(MainActivity.TgId == MainActivity.TgOn) {
                    MainActivity.FUNCTION_KIND = 2;
                    getActivity().finish();
                }
            }
        });
        return view;
    }

    public static void setDicKind(int num)
    {
        dicKind = num;
    }
    public static int getDicKind()
    {
        return dicKind;
    }

}
