package com.example.nam.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Created by Nam on 2016-10-14.
 */
public class TabFragment0 extends Fragment {
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){


        View view = inflater.inflate(R.layout.tab_fragment_0, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.main_img);
        imageView.setImageResource(R.drawable.main);

        Button button = (Button) view.findViewById(R.id.chathead_button);

        try {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainActivity.checkHead++;
                    final Intent serviceIntent = new Intent(getContext(), ChatHeadService.class);
                    serviceIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    getActivity().startService(serviceIntent);

                }
            });
        } catch(Exception e){
            Toast.makeText(getContext(),"핫키 서비스를 실행할 수 없는 휴대폰 입니다.", Toast.LENGTH_LONG);
        }


        return view;

    }
}
