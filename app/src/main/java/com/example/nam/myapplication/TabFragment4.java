package com.example.nam.myapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

/**
 * Created by Nam on 2016-10-20.
 */
public class TabFragment4 extends Fragment {
    private static int newsKind=-1;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.tab_fragment_4, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.news_img);
        imageView.setImageResource(R.drawable.news);
        Button button = (Button) view.findViewById(R.id.news_button);
        MainActivity.FUNCTION_KIND = -1;
        setNewsKind(0);

        final RadioButton naver = (RadioButton) view.findViewById(R.id.naverNews);
        final RadioButton wiki = (RadioButton) view.findViewById(R.id.daumNews);

        RadioButton.OnClickListener languageOnClickListener = new RadioButton.OnClickListener() {
            public void onClick(View v){
                if(naver.isChecked())
                    setNewsKind(0);
                else if(wiki.isChecked())
                    setNewsKind(1);

            }
        };

        naver.setOnClickListener(languageOnClickListener);
        wiki.setOnClickListener(languageOnClickListener);
        naver.setChecked(true);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(MainActivity.TgId == MainActivity.TgOff)
                    Toast.makeText(getActivity(), "ON 버튼으로 활성화 하세요", Toast.LENGTH_SHORT).show();
                else if(MainActivity.checkHead < 0)
                    Toast.makeText(getActivity(),"LAUNCH HOTKEY로 핫키를 띄워주세요",Toast.LENGTH_SHORT).show();
                else if(MainActivity.TgId == MainActivity.TgOn) {
                    MainActivity.FUNCTION_KIND = 3;
                    getActivity().finish();
                }
            }
        });
        return view;
    }

    public static void setNewsKind(int num)
    {
        newsKind = num;
    }
    public static int getNewsKind()
    {
        return newsKind;
    }
}
